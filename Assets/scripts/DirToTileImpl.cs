using System.Collections;
using UnityEngine;

class DirToTileImpl : DirToTile {
    public int DirToTile(int fromX, int fromY, int toX, int toY) {
        if (
            (fromX < 0 && toX > 0) ||
            (fromX > 0 && toX < 0)
        ) {
            return 1;
        }
        if ((fromX < 0 && toY < 0) ||
            (fromY < 0 && toX < 0)
        ) {
            return 5;
        }
        if ((fromX < 0 && toY > 0) ||
            (fromY > 0 && toX < 0)
         ) {
            return 2;
        }
        if ((fromX > 0 && toY < 0) ||
            (fromY < 0 && toX > 0)
        ) {
            return 3;
        }
        if ((fromX > 0 && toY > 0) ||
            (fromY > 0 && toX > 0)
        ) {
            return 0;
        }
        if ((fromY < 0 && toY > 0) ||
            (fromY > 0 && toY < 0)) {
            return 4;
        }
        throw new System.Exception("Invalid directions: fromX " + fromX + ", fromY " + fromY + ", toX " + toX + ", toY " + toY);
    }

    public Vector2Int[] TileToDir(int tile) {
        switch(tile) {
            case 0:
                return new Vector2Int[] {
                    new Vector2Int(0, -1),
                    new Vector2Int(1, 0)
                };
            case 1:
                return new Vector2Int[] {
                    new Vector2Int(-1, 0),
                    new Vector2Int(1, 0)
                };
            case 2:
                return new Vector2Int[] {
                    new Vector2Int(-1, 0),
                    new Vector2Int(0, -1)
                };
            case 3:
                return new Vector2Int[] {
                    new Vector2Int(0, 1),
                    new Vector2Int(1, 0)
                };
            case 4:
                return new Vector2Int[] {
                    new Vector2Int(0, 1),
                    new Vector2Int(0, -1)
                };
            case 5:
                return new Vector2Int[] {
                    new Vector2Int(-1, 0),
                    new Vector2Int(0, 1)
                };
            default:
                return new Vector2Int[] {
                    new Vector2Int(0, 0),
                    new Vector2Int(0, 0)
                };
        }
    }
}