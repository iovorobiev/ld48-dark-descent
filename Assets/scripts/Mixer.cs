using System.Collections;
using System.Collections.Generic;
using UnityEngine;
class Mixer {
    public void mix(int[,] tiles, int fI, int fJ) {
        int rI = tiles.GetLength(0) - 1;
        int rJ = tiles.GetLength(1) - 1;

        int perms = 50;
        if (fJ % 2 == (rI - fI) % 2) {
            perms++;
        }

        for (int n = 0; n < perms; n++) {
            int i1 = fI;
            int j1 = fJ;
            while (i1 == fI && j1 == fJ) {
                i1 = Random.Range(0, tiles.GetLength(0));
                j1 = Random.Range(0, tiles.GetLength(1));
            }
            
            int i2 = fI;
            int j2 = fJ;
            while (i2 == fI && j2 == fJ) {
                i2 = Random.Range(0, tiles.GetLength(0));
                j2 = Random.Range(0, tiles.GetLength(1));
            }

            int buf = tiles[i1, j1];
            tiles[i1, j1] = tiles[i2, j2];
            tiles[i2, j2] = buf;
        }
    }
}