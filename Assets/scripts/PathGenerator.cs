using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PathGenerator {
    DirToTile dirToTile;

    public PathGenerator(DirToTile dirToTile) {
        this.dirToTile = dirToTile;
    }

    public PathInfo generate(int w, int h, int fI, int fJ, int tI, int tJ) {
        int[,] result = new int[h,w];
    
        HashSet<Vector2Int> visited = new HashSet<Vector2Int>();
        List<Vector2Int> stack = new List<Vector2Int>();

        Vector2Int current = new Vector2Int(fI, fJ);

        while(current.x != tI || current.y != tJ) {
            visited.Add(current);
            List<Vector2Int> neighbors = new List<Vector2Int>();
            var top = new Vector2Int(current.x - 1, current.y);
            if (isAvailable(top, visited, w, h)) {
                neighbors.Add(top);
            }
            var bottom = new Vector2Int(current.x + 1, current.y);
            if (isAvailable(bottom, visited, w, h)) {
                neighbors.Add(bottom);
            }
            var left = new Vector2Int(current.x, current.y - 1);
            if (isAvailable(left, visited, w, h)) {
                neighbors.Add(left);
            }
            var right = new Vector2Int(current.x, current.y + 1);
            if (isAvailable(right, visited, w, h)) {
                neighbors.Add(right);
            }

            if (neighbors.Count == 0) {
                current = stack[stack.Count - 1];
                stack.RemoveAt(stack.Count - 1);
                continue;
            }
            stack.Add(current);
            current = neighbors[Random.Range(0, neighbors.Count)];
        }
        stack.Add(new Vector2Int(tI, tJ));
        int prevX = -1;
        int prevY = 0;
        int lastX = 1;
        int lastY = 0;
        for (int i = 0; i < stack.Count; i++) {
            Vector2Int cur = stack[i];
            int nextX = 0;
            int nextY = 0;
            if (i < stack.Count - 1) {
                nextX = stack[i + 1].y - cur.y;
                nextY = stack[i + 1].x - cur.x;
            } else {
                nextX = lastX;
                nextY = lastY;
            }
            result[cur.x, cur.y] = dirToTile.DirToTile(prevX, prevY, nextX, nextY);
            prevX = -nextX;
            prevY = -nextY;
        }

        Vector2Int toFix = new Vector2Int(fI,fJ);
        Vector2Int destination = new Vector2Int(tI, tJ);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Vector2Int cur = new Vector2Int(i, j);
                if (stack.Contains(cur)) {
                    continue;    
                }
                int newDist = destination.x - cur.x + destination.y - cur.y;
                int oldDist = destination.x - toFix.x + destination.y - toFix.y;
                if (newDist < oldDist) {
                    toFix = cur;
                }
            }
        }
        result[toFix.x, toFix.y] = 6;
        return new PathInfo(result, stack, toFix);
    }

    bool isAvailable(Vector2Int node, HashSet<Vector2Int> visited, int w, int h) {
        if (node.x < 0 || node.x >= h) return false;
        if (node.y < 0 || node.y >= w) return false;
        if (visited.Contains(node)) return false;
        return true;
    }

    public class PathInfo {
        public int[,] tiles;
        public List<Vector2Int> path;
        public Vector2Int toFix;

        public PathInfo(int[,] tiles, List<Vector2Int> path, Vector2Int toFix) {
            this.tiles = tiles;
            this.path = path;
            this.toFix = toFix;
        }
    }
}