using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class TypeWriterEffect : MonoBehaviour {
    public float delay = 0.1f;

    Text textComponent;
    
    public bool isAnimating = false;
    public bool stopAnimation = false;

    void Start() {
        textComponent = GetComponent<Text>();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return) && isAnimating) {
            stopAnimation = true;
        }

    }

    public IEnumerator setText(string text) {
        isAnimating = true;
        for (int i = 0; i < text.Length; i++) {
            if (stopAnimation) {
                i = text.Length - 1;
            }
            string currentText = text.Substring(0, i + 1);
            textComponent.text = currentText;
            yield return new WaitForSeconds(delay);
        }
        isAnimating = false;
        stopAnimation = false;
    }

    public void hide() {
        textComponent.text = "";
    }

}