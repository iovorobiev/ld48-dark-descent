using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Sprite[] sprites;
    SpriteRenderer spriteRenderer;    
    Vector3 targetPosition;
    public bool moving = false;

    int speed = 7;
    public int sprite;
        void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        // updateSprite(Random.Range(0, sprites.Length));
    }

    void Update() {
        if (!moving) {
            return;
        }
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
        if (transform.position == targetPosition) {
            moving = false;
        }
    }

    public void updateSprite(int sprite) {
        spriteRenderer.sprite = sprites[sprite];
        this.sprite = sprite;
    }

    public void moveTo(Vector3 newPosition) {
        targetPosition = newPosition;
        moving = true;
    }
}
