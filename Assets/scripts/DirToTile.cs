using UnityEngine;

interface DirToTile {
    public int DirToTile(int fromX, int fromY, int toX, int toY);

    public Vector2Int[] TileToDir(int tile); 
}