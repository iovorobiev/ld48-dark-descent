using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Knight : MonoBehaviour
{
    
    public int speed = 1;
    Animator animator;

    List<Vector3> path;
    int nextWaypoint;
    Vector3 finalPosition;

    public State state = State.Start;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("walk", true);
        finalPosition = new Vector3(transform.position.x + 2, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        switch(state) {
            case State.Start:
                float step1 = speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, finalPosition, step1);
                if (transform.position == finalPosition) {
                    state = State.Wait;
                    animator.SetBool("walk", false);
                }
                break;
            case State.Wait:
            break;
            case State.Ready:
                if (Input.GetKeyDown(KeyCode.Space)) {
                    animator.SetBool("walk", true);
                    state = State.Walk;
                }
                break;
            case State.Walk:
                float step = speed * Time.deltaTime;
                Vector3 targetPosition = path[nextWaypoint];
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
                if (transform.position == targetPosition) {
                    nextWaypoint++;
                    if (nextWaypoint == path.Count) {
                        state = State.Wait;
                        finishLevel();
                    }
                }
                break;
        };
    }  

    private void finishLevel() {
        PlayerStats.dungeonLevel++;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void go(List<Vector3> waypoints) {
        this.path = waypoints;
        nextWaypoint = 0;
        state = State.Ready;
    }

    public enum State {
        Start,
        Wait,
        Ready,
        Walk
    }
}
