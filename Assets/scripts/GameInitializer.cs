using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

class GameInitializer : MonoBehaviour {
    public string[] tutorial;
    int tutorialPoint = -1;
    public TypeWriterEffect tutorialText;
    public Text sanityValue;
    public Text dungeonValue;
    public GameObject[] tiles;
    public int width;
    public int height;

    public Shade shade;

    public GameObject exitTile;
    Tile[,] tiles2d;

    public Knight knight;
    DirToTile dirToTile = new DirToTileImpl();
    Vector2Int controlledTileCoords;

    private State state = State.NonMixed;
    private PathGenerator.PathInfo pathInfo;
    
     void Start()
    {
        dungeonValue.text = PlayerStats.dungeonLevel.ToString();
        sanityValue.text = getSanityReprString();
        if (PlayerStats.tutorial) {
            state = State.Tutorial;
        }
        
        tiles2d = new Tile[height, width];
        for (int i = 0; i < tiles.Length; i++) {
            int tdi = i / width;
            int tdj = i % width;
            tiles2d[tdi, tdj] = tiles[i].GetComponent<Tile>();
        }
        
        PathGenerator pg = new PathGenerator(dirToTile);
        pathInfo = pg.generate(
            tiles2d.GetLength(1), 
            tiles2d.GetLength(0), 
            0, 0, 
            tiles2d.GetLength(0) - 1, tiles2d.GetLength(1) - 1
            );
        controlledTileCoords = pathInfo.toFix;
        for (int i = 0; i < tiles2d.GetLength(0); i++) {
            for (int j = 0; j < tiles2d.GetLength(1); j++) {
                tiles2d[i, j].updateSprite(pathInfo.tiles[i, j]);
            }
        }
        
    }

    void iterateTutorial() {
        if (Input.GetKeyDown(KeyCode.Return) && !tutorialText.isAnimating) {
            tutorialPoint++;

            if (tutorialPoint == tutorial.Length) {
                state = State.Mixed;
                PlayerStats.tutorial = false;
                shade.dissolve();
                StartCoroutine(tutorialText.setText("Use arrows to arrange a path from top left corner to bottom right corner"));
            } else {
                if (tutorialPoint == 2) {
                    shade.dissolve();    
                }
                if (tutorialPoint == 3) {
                    shade.appear();
                }
                if (tutorialPoint == 4) {
                    mix();
                    shade.dissolve();
                }
                StartCoroutine(tutorialText.setText(tutorial[tutorialPoint]));
            }
        }
    }

    void mix() {
        Mixer mixer = new Mixer();
        mixer.mix(pathInfo.tiles, pathInfo.toFix.x, pathInfo.toFix.y);
        for (int i = 0; i < tiles2d.GetLength(0); i++) {
            for (int j = 0; j < tiles2d.GetLength(1); j++) {
                tiles2d[i, j].updateSprite(pathInfo.tiles[i, j]);
            }
        }
    }

    bool tileMoving;
    void Update() {
        if (state == State.Lost) {
            if (Input.GetKeyDown(KeyCode.Return)) {
                PlayerStats.dungeonLevel = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            return;
        }
        if (state == State.Tutorial) {
            if (knight.state == Knight.State.Wait && tutorialPoint == -1) {
                tutorialPoint++;
                StartCoroutine(tutorialText.setText(tutorial[tutorialPoint]));
            }
            iterateTutorial();
            return;
        }
        if (state == State.Done) return;
        if (state == State.NonMixed) {
            if (!PlayerStats.tutorial && knight.state == Knight.State.Wait) {
                StartCoroutine(tutorialText.setText("Ah... the aura of this place makes me sick"));
                mix();
                shade.dissolve();
                state = State.Mixed;
            }
            return;
        }
        if (tiles2d[controlledTileCoords.x, controlledTileCoords.y].moving) {
            tileMoving = true;
            return;
        } else if (tileMoving) {
            checkForSolution();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            if (controlledTileCoords.y == tiles2d.GetLength(1) - 1) {
                return;
            }
            swap(controlledTileCoords, controlledTileCoords.x, controlledTileCoords.y + 1);
        } else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            if (controlledTileCoords.y == 0) {
                return;
            }
            swap(controlledTileCoords, controlledTileCoords.x, controlledTileCoords.y - 1);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            if (controlledTileCoords.x == 0) {
                return;
            }
            swap(controlledTileCoords, controlledTileCoords.x - 1, controlledTileCoords.y);
        } else if (Input.GetKeyDown(KeyCode.DownArrow)) {
            if (controlledTileCoords.x == tiles2d.GetLength(0) - 1) {
                return;
            }
            swap(controlledTileCoords, controlledTileCoords.x + 1, controlledTileCoords.y);
        }
    }

    void swap(Vector2Int controlled, int i, int j) {
        PlayerStats.sanity--;
        if (PlayerStats.sanity == 0) {
            triggerEndGame();
        }
        
        sanityValue.text = getSanityReprString();
        Tile swapWith = tiles2d[i, j];
        Tile control = tiles2d[controlled.x, controlled.y];
        tiles2d[i, j] = control;
        tiles2d[controlled.x, controlled.y] = swapWith;
        var swapPos = swapWith.transform.position;
        swapWith.moveTo(control.transform.position);
        control.moveTo(swapPos);
        controlledTileCoords = new Vector2Int(i, j);
    }

    string getSanityReprString() {
        return (PlayerStats.sanity / 5).ToString();
    }
    
    void triggerEndGame() {
        shade.appear();
        StartCoroutine(tutorialText.setText("You spent to much time trying to figure out your way in this dark maze. You became insane on level " + PlayerStats.dungeonLevel + " of the dungeon. [Press Enter to restart]"));
        state = State.Lost;
    }

    void checkForSolution() {
        HashSet<Vector2Int> visited = new HashSet<Vector2Int>();
        List<Vector2Int> stack = new List<Vector2Int>();
        Tile start = tiles2d[0,0];
        var startDirs = dirToTile.TileToDir(start.sprite);
    
        if (startDirs[0] != new Vector2Int(-1, 0) && 
            startDirs[1] != new Vector2Int(-1, 0)) {
            return;
        }
        var endDirs = dirToTile.TileToDir(tiles2d[height - 1, width - 1].sprite);
        if (endDirs[0] != new Vector2Int(1, 0) && 
            endDirs[1] != new Vector2Int(1, 0)) {
            return;
        }
        visited.Add(controlledTileCoords);
        var current  = new Vector2Int(0, 0);
        // x = i, y = j
        while((current.x != height - 1 || current.y != width - 1)) {
            visited.Add(current);
            Tile tile = tiles2d[current.x, current.y];
            var top = new Vector2Int(current.x - 1, current.y);
            List<Vector2Int> neighbors = new List<Vector2Int>();
            if (isAvailable(current, top, visited, width, height)) {
                neighbors.Add(top);
            }
            var bottom = new Vector2Int(current.x + 1, current.y);
            if (isAvailable(current, bottom, visited, width, height)) {
                neighbors.Add(bottom);
            }
            var left = new Vector2Int(current.x, current.y - 1);
            if (isAvailable(current, left, visited, width, height)) {
                neighbors.Add(left);
            }
            var right = new Vector2Int(current.x, current.y + 1);
            if (isAvailable(current, right, visited, width, height)) {
                neighbors.Add(right);
            }

            if (neighbors.Count == 0) {
                if (stack.Count == 0) break;
                current = stack[stack.Count - 1];
                stack.RemoveAt(stack.Count - 1);
                continue;
            }
            stack.Add(current);
            current = neighbors[Random.Range(0, neighbors.Count)];
        }
        if (stack.Count > 0) {
            stack.Add(new Vector2Int(height - 1, width - 1));
            List<Vector3> path = new List<Vector3>();
            for (int i = 0; i < stack.Count; i++) {
                var position = tiles2d[stack[i].x, stack[i].y].transform.position;
                Debug.Log(position);
                path.Add(position);
            }
            Debug.Log("Path length " + path.Count);
            path.Add(exitTile.transform.position);

            knight.go(path);
            StartCoroutine(tutorialText.setText("Oh, I see it much clearer now! [Press enter to descend deeper]"));
            state = State.Done;
        }
    }

    bool isAvailable(Vector2Int cur, Vector2Int node, HashSet<Vector2Int> visited, int width, int height) {
        if (node == controlledTileCoords) return false;
        if (node.x < 0 || node.x >= height) return false;
        if (node.y < 0 || node.y >= width) return false;
        if (visited.Contains(node)) return false;
        Tile nodeTile = tiles2d[node.x, node.y];
        Tile curTile = tiles2d[cur.x, cur.y];
        // -1, 0 - cur to the left => cur must have right (1, 0) and next must have left (-1, 0)
        //  1, 0 - cur to the right => cur must have left (-1, 0) and next must have right (1, 0)
        //  0, 1 - cur to the top => cur must have bottom (0, -1) and next must have top (0, 1)
        //  0, -1 - cur to the bottom => cur must have top (0, 1) and next must have bottom (0, -1)
        // cur must have (-x, -y) and next must have (x, y)
        Vector2Int diff = new Vector2Int(cur.y - node.y, node.x - cur.x);
        Vector2Int[] curDirs = dirToTile.TileToDir(curTile.sprite);
        Vector2Int[] nodeDirs = dirToTile.TileToDir(nodeTile.sprite);
        Vector2Int curNeededDiff = new Vector2Int(-diff.x, -diff.y);

        return (
            curDirs[0] == curNeededDiff ||
            curDirs[1] == curNeededDiff
            ) && (
                nodeDirs[0] == diff ||
                nodeDirs[1] == diff
            );
    }

    enum State {
        Tutorial,
        NonMixed,
        Mixed,
        Done,
        Lost,
    }
}