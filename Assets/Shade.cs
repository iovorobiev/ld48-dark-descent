using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shade : MonoBehaviour
{
    Material material;
    float fade = 1.0f;

    bool isDissolving = false;
    bool isAppearing = false;
    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
    }

    void Update() {
        if (isDissolving) {
            fade -= Time.deltaTime;

            if (fade <= 0f) {
                fade = 0f;
                isDissolving = false;
            }
            material.SetFloat("_Fade", fade);
        } else if (isAppearing) {
            fade += Time.deltaTime;

            if (fade >= 1f) {
                fade = 1f;
                isAppearing = false;
            }
            material.SetFloat("_Fade", fade);
        }
    }

    public void dissolve() { 
        isDissolving = true;
    }

    public void appear() {
        isAppearing = true;
    }
}
